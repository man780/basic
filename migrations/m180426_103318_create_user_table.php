<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180426_103318_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(),
            'login' => $this->string(32),
            'password' => $this->string()->notNull(),
            'auth_key' => $this->string(),
        ]);
		
		$this->insert('user',array(
			'fio'=>'Иван Иванов',
			'login' =>'admin',
			'password' => md5('admin'),
            'auth_key' => null,
		));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
